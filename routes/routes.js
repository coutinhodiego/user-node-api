const express = require('express')
const router = express.Router()

const userController = require('../controllers/userController')

router.post('/user-api/registeruser', userController.registerUser)
router.get('/user-api/getall', userController.getAll)
router.get('/user-api/getuser/:id', userController.getUser)
router.put('/user-api/updateuser/:id', userController.updateUser)
router.delete('/user-api/removeuser/:id', userController.removeUser)

module.exports = router