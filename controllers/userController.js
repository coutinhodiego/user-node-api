const User = require('../models/UserSchema')
const bcrypt = require('bcryptjs')


exports.getAll = async (req, res, next) => {

    let logedUserId = req.body.user

    // console.log('************************************************************************************************************************************')
    // console.log(`**************** O Usuario logado de ID: ${logedUserId} esta fazendo a requisicao apos ser autenticado!. *****************`)
    // console.log('************************************************************************************************************************************')
    
    try {
        let users = await User.find()
        let logedUser = await User.findById(logedUserId).populate()
        
        return res.json({
            status: 200,
            logedUser: logedUser,
            data: users
        })

    } 
    catch (err) {

        return res.json({
            status: 404,
            msg : 'Dados nao encontrados'
        })
    }

}

exports.getUser = async (req, res, next) => {
    
    let userId = req.params.id

    try {

        let user = await User.findById(userId)

        return res.json({
            status: 200,
            data: user
        })

    } 
    catch (err) {
        
        return res.json({
            status: 404,
            msg : 'Dados nao encontrados'
        })
    }
}

exports.registerUser = async (req, res, next) => {
    // console.log(req.body)
    try {

        let user = new User(req.body)
        user.password = await bcrypt.hashSync(req.body.password, 10)
        
        await user.save()
        
        return res.json({
            status: 201,
            data: user
        })
    
    } 
    catch (err) {

        return res.json({
            status: 401,
            msg : 'Ocorreu um erro no processo.'
        })
    }
}

exports.updateUser = async (req, res, next) => {
    
    let userId = req.params.id
    let data = req.body
    
    try {

        let userUpdated = await User.findByIdAndUpdate(userId, data)
        
        return res.json({
            status: 200,
            data: userUpdated
        })
    
    } 
    catch (err) {

        return res.json({
            status: 404,
            msg : 'Dados nao encontrados'
        })
    }
    

}

exports.removeUser = async (req, res, next) => {

    let userId = req.params.id

    try {

        let user = await User.findByIdAndRemove(userId)
        
        return res.json({
            status: 200,
            data: user
        })

    } catch (err) {

        return res.json({
            status: 404,
            msg : 'Dados nao encontrados'
        })   
    }
}