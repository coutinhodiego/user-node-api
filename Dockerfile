FROM node:alpine

MAINTAINER Diego Coutinho

ENV PORT=3001
ENV MONGO_DB=mongodb://localhost/user-node-api

COPY . /node/api

EXPOSE $PORT

WORKDIR /node/api

RUN npm install

CMD ["npm", "start"]


